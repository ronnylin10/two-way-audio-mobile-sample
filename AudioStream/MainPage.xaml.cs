﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.AudioRecorder;
using System.Threading;
using Sockets.Plugin;

namespace AudioStream
{
    public partial class MainPage : ContentPage
    {
        AudioRecorderService recorder;
        AudioPlayer player;
        bool call_state;
        CancellationTokenSource cts;
        private readonly TcpSocketClient _client;
        private int BUF_SIZE = 1024;
        private string REMOTE_HOST_IP = "192.168.10.203";
        private int REMOTE_HOST_PORT = 9876;
        int bytesRead;
        byte[] buf;

        public MainPage()
        {
            InitializeComponent();

            recorder = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };

            _client = new TcpSocketClient();
            bytesRead = -1;
            buf = new byte[BUF_SIZE];
            player = new AudioPlayer();
            player.FinishedPlaying += Player_FinishedPlaying;
            call_state = false;
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void Record_Clicked(object sender, EventArgs e)
        {
            await RecordAudio();
        }

        async Task RecordAudio()
        {
            try
            {
                if (!recorder.IsRecording) //Record button clicked
                {
                    
                    RecordButton.IsEnabled = false;
                    PlayButton.IsEnabled = false;

                    //start recording audio
                    var audioRecordTask = await recorder.StartRecording();

                    RecordButton.Text = "Stop Recording";
                    RecordButton.IsEnabled = true;

                    await audioRecordTask;

                    RecordButton.Text = "Record";
                    PlayButton.IsEnabled = true;
                }
                else //Stop button clicked
                {
                    RecordButton.IsEnabled = false;

                    //stop the recording...
                    await recorder.StopRecording();

                    RecordButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                //blow up the app!
                throw ex;
            }
        }

        void Init_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<AudioStreamPlayer>().Init();
        }

        void AudioIn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<AudioStreamPlayer>().AudioIn();
        }
        void AudioOut_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<AudioStreamPlayer>().AudioOut();
        }
        void Pause_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<AudioStreamPlayer>().Pause();
        }
        void Dispose_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<AudioStreamPlayer>().Dispose();
        }


        void PlayAudio()
        {
            try
            {
                var filePath = recorder.GetAudioFilePath();

                if (filePath != null)
                {
                    PlayButton.IsEnabled = false;
                    RecordButton.IsEnabled = false;

                    player.Play(filePath);
                }
            }
            catch (Exception ex)
            {
                //blow up the app!
                throw ex;
            }
        }

        async void Call_Clicked(object sender, EventArgs e)
        {
            
            //connect call if not calling
            if(!call_state)
            {
                try
                {
                    call_state = true;
                    CallButton.IsEnabled = false;
                    A_timer.IsVisible = true;
                    A_timer.Text = "Connecting";
                    await _client.ConnectAsync(REMOTE_HOST_IP, REMOTE_HOST_PORT);
                }
                catch (Exception error)
                {
                    System.Diagnostics.Debug.Write(error);
                    A_timer.Text = "Server not available";
                    call_state = false;
                    CallButton.Text = "Call";
                    CallButton.IsEnabled = true;
                    return;
                }
                CallButton.Text = "Hang Up";
                CallButton.IsEnabled = true;
                cts = new CancellationTokenSource();
                A_timer.Text = "Call Connected";
                ReadByte(cts.Token);

            }
            else
            {
                CallButton.IsEnabled = false;
                cts.Cancel();
                CallButton.IsEnabled = true;
                call_state = false;
            }

            //start streaming audio input
            //start playing audio
        }

        async void ReadByte(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.Write("connected");
            try
            {
                while (true)
                {
                    System.Diagnostics.Debug.Write("bytesRead at beginning: " + bytesRead + "\n");
                    System.Diagnostics.Debug.Write("Reading bytes\n");
                    bytesRead = await _client.ReadStream.ReadAsync(buf, 0, BUF_SIZE);
                    if (bytesRead > 0)
                    {
                        System.Diagnostics.Debug.Write("bytesRead: " + bytesRead + "\n");
                        System.Diagnostics.Debug.Write("buf[0]: " + buf[0] + "\n");
                        await _client.WriteStream.WriteAsync(buf, 0, bytesRead);
                    }

                    if (cancellationToken.IsCancellationRequested)
                    { 
                        CallButton.IsEnabled = false;
                        await _client.DisconnectAsync();
                        call_state = false;
                        CallButton.Text = "Call";
                        CallButton.IsEnabled = true;
                        A_timer.IsVisible = false;
                        System.Diagnostics.Debug.Write("disconnected\n");
                        return;
                    } 
                }
                CallButton.IsEnabled = false;
                await _client.DisconnectAsync();
                System.Diagnostics.Debug.Write("at read end disconnected\n");
                call_state = false;
                CallButton.Text = "Call";
                CallButton.IsEnabled = true;
                A_timer.IsVisible = false;

            }
            catch (Exception error)
            {
                System.Diagnostics.Debug.Write(error);
                System.Diagnostics.Debug.Write("reached end of file on server\n");
                CallButton.IsEnabled = false;
                await _client.DisconnectAsync();
                System.Diagnostics.Debug.Write("at end file disconnected\n");
                call_state = false;
                CallButton.Text = "Call";
                CallButton.IsEnabled = true;
                A_timer.IsVisible = false;
            }

        }

        //async Task<TcpSocketClient> Connect_Call()
        //{
        //    // TODO define states of call, i.e. not on call; connecting to call; in call.
        //    if(!call_state)
        //    {
        //        cts = new CancellationTokenSource();
        //        call_state = true;
        //        CallButton.IsEnabled = false;
        //        //Task<bool> status = Do_connect_stuff(cts.Token);
        //        Task<TcpSocketClient> tcpclient = TCPClient_connect(cts.Token);

        //        CallButton.Text = "Hang Up";
        //        CallButton.IsEnabled = true;

        //        bool connected = await tcpclient != null;

        //        if (connected) 
        //        {
        //            A_timer.Text = "Call Connected";

        //        }
        //        else 
        //        {
        //            A_timer.Text = "Call did not connect";
        //            call_state = false;
        //            CallButton.Text = "Call";
        //            CallButton.IsEnabled = true;
        //        }

        //        return tcpclient;
        //    }
        //    else
        //    {
                
        //        CallButton.IsEnabled = false;
        //        cts.Cancel();
        //        CallButton.IsEnabled = true;
        //        call_state = false;
        //    }


        //}

        //async Task<TcpSocketClient> TCPClient_connect(CancellationToken cancellationToken)
        //{
        //    System.Diagnostics.Debug.Write("connecting");
        //    A_timer.Text = "Connecting";
        //    var tcpclient = new TcpSocketClient();
        //    await tcpclient.ConnectAsync("192.168.51.15", 8080);
        //    return tcpclient;
        //    var bytesRead = -1;
        //    var buf = new byte[512];

        //    System.Diagnostics.Debug.Write("connected");
        //    while (bytesRead != 0)
        //    {
        //        System.Diagnostics.Debug.Write("in while loop\n");
        //        bytesRead = await tcpclient.ReadStream.ReadAsync(buf, 0, 512);
        //        if (bytesRead > 0)
        //            System.Diagnostics.Debug.Write(buf[0]+'\n');
        //        if (cancellationToken.IsCancellationRequested)
        //        {
        //            await tcpclient.DisconnectAsync();
        //            System.Diagnostics.Debug.Write("disconnected");
        //            return false;
        //        }
        //    }

        //    System.Diagnostics.Debug.Write("nothing to read?");
        //    return true;
        //}
       

        async Task<bool> Do_connect_stuff(CancellationToken cancellationToken)
        {

            A_timer.IsVisible = true;
            for (int i = 0; i < 10; i++)
            {   
                if (cancellationToken.IsCancellationRequested)
                {
                    return false;
                }

                A_timer.Text = "Connecting please wait ... " + i.ToString();
                await Task.Delay(1000);
            }
            return true;
        }

        async void SecondCall_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CallPage());
        }

        void Player_FinishedPlaying(object sender, EventArgs e)
        {
            PlayButton.IsEnabled = true;
            RecordButton.IsEnabled = true;
        }
    }
}
