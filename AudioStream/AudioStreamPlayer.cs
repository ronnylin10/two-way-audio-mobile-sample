﻿using System;
namespace AudioStream
{
    public interface AudioStreamPlayer
    {
        void Init();

        void AudioIn();

        void AudioOut();

        void Pause();

        void Dispose();
    }
}
