﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.AudioRecorder;

namespace AudioStream
{
    public partial class CallPage : ContentPage
    {
        
        AudioRecorderService recorder;
        AudioPlayer player;
        bool call_state;
        CancellationTokenSource cts;
        ImageSource[] sprites = new ImageSource[163];

        public CallPage()
        {
            InitializeComponent();

            recorder = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };

            player = new AudioPlayer();

            InitializeComponent();
            LoadSplashImages();

        }

        public void StreamAndReceiveAudio()
        {
            
        }

        async Task Connect_Call()
        {
            // TODO define states of call, i.e. not on call; connecting to call; in call.
            if (!call_state)
            {
                cts = new CancellationTokenSource();
                call_state = true;

                Task<bool> status = Do_connect_stuff(cts.Token);

                bool connected = await status;

                if (connected)
                {
                    Message.Text = "Call Connected";
                }
                else
                {
                    Message.Text = "Call did not connect";
                    call_state = false;
                }
            }
            else
            {
                cts.Cancel();
                call_state = false;
            }
        }

        async Task Connect_Call_new()
        {
            cts = new CancellationTokenSource();
            Task<bool> status = Do_connect_stuff(cts.Token);
            bool connected = await status;

            if (connected)
            {
                Message.Text = "Call Connected";
            }
            else
            {
                Message.Text = "Call did not connect, going back to previous menu";
            }
        }

        async Task<bool> Do_connect_stuff(CancellationToken cancellationToken)
        {
            Message.IsVisible = true;
            for (int i = 0; i < 10; i++)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return false;
                }
                Message.Text = "Connecting please wait ... " + i.ToString();
                await Task.Delay(1000);
            }
            return true;
        }

        public void EndCall_Clicked(object sender, EventArgs e)
        {
            EndCall();
        }

        public void EndCall()
        {
            EndCallButton.IsEnabled = false;
            cts.Cancel();
            cts.Dispose();
            Navigation.PopAsync();
        }



        void LoadSplashImages()
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                String num = i.ToString("D5");
                ImageSource ims = ImageSource.FromFile("Resources/Loading" + num + ".png");
                sprites[i] = ims;
            }
        }

        void PlayAni()
        {
            //aniImage.Source = sprites[0];
            //int nextFrame = 1;
            //Device.StartTimer(TimeSpan.FromMilliseconds(80), () => {
            //    //  Device.BeginInvokeOnMainThread(() => { aniImage.Source = sprites[nextFrame]; });
            //    aniImage.Source = sprites[nextFrame];
            //    nextFrame++;
            //    if (nextFrame == sprites.Length) nextFrame = 0;
            //    return true;
            //});
        }

        private void SplashIntro_Appearing(object sender, EventArgs e)
        {
            PlayAni();
        }

    }
}
