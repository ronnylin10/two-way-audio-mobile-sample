﻿using System;
using Foundation;
using AudioStream.iOS;
using AudioToolbox;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using CoreFoundation;
using System.Threading.Tasks;
using AVFoundation;

[assembly: Xamarin.Forms.Dependency(typeof(AudioStreamPlayer_iOS))]
namespace AudioStream.iOS
{
    public class AudioStreamPlayer_iOS : AudioStreamPlayer
    {
        bool outputQueueStarted;
        bool inputQueueStarted;
        AudioFileStream audioFileStream;
        InputAudioQueue inputQueue;
        OutputAudioQueue outputQueue;
        NSUrlSessionStreamTask dataTask;
        private readonly string REMOTE_HOST_IP = "192.168.51.15";
        //private readonly string REMOTE_HOST_IP = "192.168.51.200";
        //private readonly string REMOTE_HOST_IP = "10.0.9.255";
        private readonly int REMOTE_HOST_PORT = 8080;
        private double _samplingRate = 44100;
        private readonly int BUF_SIZE = 1024;
        private readonly nuint _read_BUFSIZE = 512;
        public NSTimer timer { get; private set; }

        public void Init()
        {
            var session = AVAudioSession.SharedInstance();
            if (session == null)
            {
                Debug.WriteLine("session unavailable");
            }
            else
            {
                NSError error;
                error = session.SetCategory(AVAudioSessionCategory.PlayAndRecord, AVAudioSessionCategoryOptions.DefaultToSpeaker);
                Debug.WriteLine("set category error:" + error);
                //session.SetMode(AVAudioSession.ModeVoiceChat, out error);
                //var success = session.OverrideOutputAudioPort(AVAudioSessionPortOverride.Speaker, out error);
            }
            audioFileStream = new AudioFileStream(AudioFileType.AAC_ADTS);
            audioFileStream.PacketDecoded += OnPacketDecoded;
            audioFileStream.PropertyFound += OnPropertyFound;
            AudioClassDescription[] encoders = AudioFormatAvailability.GetEncoders(AudioFormatType.MPEG4AAC);
            var isavailable = encoders.Any(encoder => encoder.SubType == AudioFormatType.MPEG4AAC);
            Debug.WriteLine("is encoder available: "+isavailable);

            var s = NSUrlSession.FromConfiguration(NSUrlSessionConfiguration.DefaultSessionConfiguration, (new SessionDelegate(audioFileStream) as INSUrlSessionDelegate), NSOperationQueue.MainQueue);
            dataTask = s.CreateBidirectionalStream(REMOTE_HOST_IP, REMOTE_HOST_PORT);
            if (dataTask.Error != null)
            {
                Debug.WriteLine("Error occured on creating task");
                Debug.WriteLine(dataTask.Error);
                return;
            }
            dataTask.Resume();
            Debug.WriteLine("datatask state:" + dataTask.State);
            Parallel.Invoke(() => AudioIn(), () => AudioOut());

            //this.timer = NSTimer.CreateScheduledTimer(5, (_) => outputQueue.Start());
            //AudioOut();
        }

        class SessionDelegate : NSUrlSessionStreamDelegate, INSUrlSessionDelegate
        {
            readonly AudioFileStream audioFileStream;

            public SessionDelegate(AudioFileStream audioFileStream)
            {
                this.audioFileStream = audioFileStream;
            }

            [Export("URLSession:task:didCompleteWithError:")]
            public override void DidCompleteWithError(NSUrlSession session, NSUrlSessionTask task, NSError error)
            {
                Debug.WriteLine("DidCompError: " + error);
                Debug.WriteLine("error code:" + error.Code);
            }

            [Export("URLSessionDidFinishEventsForBackgroundURLSession:")]
            public override void DidFinishEventsForBackgroundSession(NSUrlSession session)
            {
                Debug.WriteLine("Didfinishevent: " + session.DebugDescription);
            }
        }

        public void AudioIn()
        {
            inputQueue = new InputAudioQueue(new AudioStreamBasicDescription()
            {
                SampleRate = _samplingRate,
                Format = AudioFormatType.MPEG4AAC,
                ChannelsPerFrame = 1, // monoral
            },CFRunLoop.Current,CFRunLoop.ModeDefault);
            inputQueue.InputCompleted += InputQueue_InputCompleted;
            var allocaStatus = inputQueue.AllocateBufferWithPacketDescriptors(BUF_SIZE, BUF_SIZE, out IntPtr bufferPtr);
            //var allocaStatus2 = inputQueue.AllocateBuffer(BUF_SIZE, out bufferPtr);
            //var allocaStatus3 = inputQueue.AllocateBuffer(1024, out IntPtr buffer);
            Debug.WriteLine("allocaStatus: " + allocaStatus);
            var enqueueStatus = inputQueue.EnqueueBuffer(bufferPtr, BUF_SIZE, null);
            Debug.WriteLine("enqueueStatus: " + enqueueStatus);
            var startStatus = inputQueue.Start(); 
            Debug.WriteLine("startStatus: " + startStatus);
            inputQueueStarted = true;
        }

        private async void InputQueue_InputCompleted(object sender, InputCompletedEventArgs e)
        {
            var buffer = (AudioQueueBuffer)Marshal.PtrToStructure(e.IntPtrBuffer, typeof(AudioQueueBuffer));
            int outputPacketSize = (int)buffer.AudioDataByteSize + 7;
            byte[] sendbuf = new byte[outputPacketSize];
            AddADTStoPacket(sendbuf, outputPacketSize);
            Marshal.Copy(buffer.AudioData,sendbuf,7,(int)buffer.AudioDataByteSize);
            await dataTask.WriteDataAsync(NSData.FromArray(sendbuf), 30);
            Debug.WriteLine("sendbuff:" + sendbuf.Length);
            inputQueue.EnqueueBuffer(e.IntPtrBuffer, BUF_SIZE, e.PacketDescriptions);
        }

        private void AddADTStoPacket(byte[] packet, int databytelength)
        {
            int MPEG_profile = 1;
            int freqIdx = 4;
            int channel_config = 1;
            packet[0] = (byte)0xFF;
            packet[1] = (byte)0xF9;
            packet[2] = (byte)(((MPEG_profile - 1) << 6) + (freqIdx << 2) + (channel_config >> 2));
            packet[3] = (byte)(((channel_config & 3) << 6) + (databytelength >> 11));
            packet[4] = (byte)((databytelength & 0x7FF) >> 3);
            packet[5] = (byte)(((databytelength & 7) << 5) + 0x1F);
            packet[6] = (byte)0xFC;
        }

        public async void AudioOut()
        {
            bool isReading = true;

            while (isReading)
            {
                NSUrlSessionStreamDataRead data_in = await dataTask.ReadDataAsync(0, _read_BUFSIZE, 30);
                if (data_in.AtEof == true)
                {
                    Debug.WriteLine("EoF is true, disposing audioqueue");
                    Debug.WriteLine("is outpus audio queue running still: " + outputQueue.IsRunning);
                    isReading = false;
                    return;
                }
                Debug.WriteLine("read packet debug description:"+data_in.Data.DebugDescription);
                audioFileStream.ParseBytes((int)data_in.Data.Length, data_in.Data.Bytes, false);
            }
        }

        void OnPacketDecoded(object sender, PacketReceivedEventArgs e)
        {
            Debug.WriteLine("Audiofilestream Status: "+ audioFileStream.LastError);
            outputQueue.AllocateBuffer(e.Bytes, out IntPtr outBuffer);
            AudioQueue.FillAudioData(outBuffer, 0, e.InputData, 0, e.Bytes);
            outputQueue.EnqueueBuffer(outBuffer, e.Bytes, e.PacketDescriptions);
            if (!outputQueueStarted)
            {
                var status = outputQueue.Start();
                if (status != AudioQueueStatus.Ok)
                {
                    Debug.WriteLine("could not start audio queue");
                }
                Debug.WriteLine("outputqueue started");
                outputQueueStarted = true;
            }
        }

        void OnPropertyFound(object sender, PropertyFoundEventArgs e)
        {
            Debug.WriteLine("Audiofilestream status on property found:" + audioFileStream.LastError);
            if (e.Property == AudioFileStreamProperty.ReadyToProducePackets)
            {
                outputQueue = new OutputAudioQueue(audioFileStream.StreamBasicDescription);
                outputQueue.BufferCompleted += OutputQueue_BufferCompleted;
                Debug.WriteLine("volume:" + outputQueue.Volume);
            }
            else 
            {
                Debug.WriteLine("property not ready to produce packets:" + e.Property);
            }
        }

        private void OutputQueue_BufferCompleted(object sender, BufferCompletedEventArgs e)
        {
            Debug.WriteLine("converter error happening?:" + outputQueue.ConverterError);
            outputQueue.FreeBuffer(e.IntPtrBuffer);
        }

        public void Dispose()
        {
            if (outputQueueStarted == true)
            {
                var status = outputQueue.Stop(true);
                outputQueue.Flush();
                Debug.WriteLine("stop status output: " + status);
                outputQueue.Dispose();
                outputQueueStarted = false;
            }

            if (inputQueueStarted == true)
            {
                var status = inputQueue.Stop(true);
                inputQueue.Flush();
                Debug.WriteLine("stop statu input: " + status);
                inputQueue.Dispose();
                inputQueueStarted = false;
            }
            dataTask.Cancel();
            audioFileStream.Dispose();
            dataTask.Dispose();
            Debug.WriteLine("datatask state:" + dataTask.State);
        }

        public void Pause()
        {
            if (outputQueueStarted == true)
            {
                var status = outputQueue.Pause();
                Debug.WriteLine("stop status output: " + status);
                outputQueue.Dispose();
            }

            if (inputQueueStarted == true)
            {
                var status = inputQueue.Pause();
                Debug.WriteLine("stop statu input: " + status);
                inputQueue.Dispose();
            }
            dataTask.Suspend();
            Debug.WriteLine("datatask state:" + dataTask.State);
        }
    }
}
